// @noflow

import React from 'react';
import Button from '@material-ui/core/Button';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';

import './common.css';

const CheckoutForm = (props) => {
    const stripe = useStripe();
    const elements = useElements();

    const handleSubmit = async (event) => {
        // Block native form submission.
        event.preventDefault();

        if (!stripe || !elements) {
            // Stripe.js has not loaded yet. Make sure to disable
            // form submission until Stripe.js has loaded.
            return;
        }

        // Get a reference to a mounted CardElement. Elements knows how
        // to find your CardElement because there can only ever be one of
        // each type of element.
        const cardElement = elements.getElement(CardElement);

        // Use your card Element with other Stripe.js APIs
        const {error, paymentMethod} = await stripe.createPaymentMethod({
            type: 'card',
            card: cardElement,
        });

        if (error) {
            alert(error.message);
        } else {
            props.captureStripeTokenResponse(paymentMethod);
        }
    };

    const styles = {
        formStyle: {
            maxWidth: '500px',
            width: '100%',
            margin: '0 auto'
        }
    };

    return (
        <form style={styles.formStyle} onSubmit={handleSubmit}>
            <CardElement
                options={{
                    style: {
                        base: {
                            fontSize: '16px',
                            color: '#424770',
                            '::placeholder': {
                                color: '#aab7c4',
                            },
                        },
                        invalid: {
                            color: '#9e2146',
                        },
                    },
                }}
            />
            <Button
                variant="contained"
                color="primary"
                type="submit"
                disabled={!stripe}
            >
                CHECKOUT
            </Button>
        </form>
    );
};

export default CheckoutForm;

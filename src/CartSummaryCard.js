import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export default function CartSummaryCard(props) {
    const { item_purchase_count, transaction_fees, cart_total, stripe_cart_total } = props;
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography variant="body2" component="p">Total Items = {item_purchase_count}</Typography>
                <Typography variant="body2" component="p">Fees = {transaction_fees}</Typography>
                <Typography variant="body2" component="p">Subtotal = {cart_total}</Typography>
                <Typography variant="h5" component="h2">Cart Total = {stripe_cart_total}</Typography>
            </CardContent>
        </Card>
    );
}

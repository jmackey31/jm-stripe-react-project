import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import axios from 'axios';

import StripeCardCheckout from './StripeCardCheckout';
import CartSummaryCard from './CartSummaryCard';

import './App.css';

const test_stripe_key = 'stripe_pk_test_value_here';
const stripePromise = loadStripe(test_stripe_key);

const App = () => {
    const item_purchase_count = 2;
    const transaction_fees = 300;
    const cart_total = 2600;
    const stripe_cart_total = cart_total + transaction_fees;
    // NOTE: ----------------------------------------------------------------------------------------
    // Setting these items manually but this should be pulled from a database or client side storage.
    const billers_name = 'Billy Withers';
    const customer_id = 'cus_some_value_here';
    // ----------------------------------------------------------------------------------------------

    const submitPaymentRequest = async cardElement => {
        try {
            const { stripe_token_card_id } = cardElement;

            axios.post('http://localhost:8700/api_v1/handle-entire-payment', {
                currency: 'usd',
                stripe_cart_total,
                stripe_token_card_id,
                cardElement,
                customer_id,
                billers_name
            })
            .then(data_response => {
                console.log('---------------- START data_response');
                console.log(data_response);
                console.log('data_response END ------------------');
                const { success, message } = data_response.data;

                if (!success) {
                    alert(message);
                } else {
                    alert('All set!');
                }
            })
            .catch(e => {
                alert('Sorry, could not complete this action.');

                throw e;
            });
        } catch (e) {
            alert(JSON.stringify(e));
        }
    };

    const captureStripeTokenResponse = cardElement => {
        if (cardElement && cardElement.id) {
            submitPaymentRequest(cardElement).then(response => response);
        } else {
            alert('Something went wrong while processing your card.  Please try again.');
        }
    };

    return (
        <div className="App">
            <h2>JM React Stripe Project</h2>
            <div>
                <p>
                    The purpose of this project is to test out stripe payments via stripe's
                    <strong>"Payment Intents"</strong> api:
                    <a href="https://stripe.com/docs/payments/payment-intents" target="_blank">
                        https://stripe.com/docs/payments/payment-intents
                    </a>
                </p>
                <hr/>
            </div>

            <CartSummaryCard
                item_purchase_count={item_purchase_count}
                transaction_fees={transaction_fees}
                cart_total={cart_total}
                stripe_cart_total={stripe_cart_total}
            />
            <div>
                <i>Note: Use a sample test card number like so...</i>
                <ul>
                    <li>Test card number: 4242 4242 4242 4242</li>
                    <li>Month: 12</li>
                    <li>Year: 27</li>
                    <li>CVC: 839</li>
                    <li>Zip Code: 90210</li>
                </ul>
            </div>

            <div>
                <Elements stripe={stripePromise}>
                    <StripeCardCheckout captureStripeTokenResponse={captureStripeTokenResponse} />
                </Elements>
            </div>
        </div>
    );
};

export default App;
